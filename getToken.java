import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileReader;
import java.io.InputStreamReader;

class getToken
{
	public static void main(String[] args)
	{
		boolean stream = true;
		boolean file = false;
		String filePath = null;
		boolean help = false;

		for (int i = 0; i <args.length; i++) 
		{
			if (args[i] == "file") 
			{
				stream = false;

				int next = ++i;
				if (args.length <= next) 
				{
					help = true;
				} else {
					filePath = args[next];
				}

				return;
			} else if (args[i] == "help") {
				help = true;
			}
		}

		BufferedReader read = null;

		if(help)
		{
			showHelp();
			return; 
		}
	}

	public static void showHelp(boolean x)
	{
		String help = 	"\n" +
						"file, read from file" + "\n" +
						"help, show help." + "\n\n" +
						"example: " + "\n\n" +
						"getToken file token.dat";

		if(x)
		{
			System.err.print(help);
		} else {
			System.out.print(help);
		}
	}
}