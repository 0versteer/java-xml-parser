import java.util.HashMap;

public class TokenValues
{
	private HashMap<String, Integer> map;

	public TokenValues()
	{
		this.map = new HashMap<String, Integer>();

		this.map.put("IF", 1);
		this.map.put("THEN", 2);
		this.map.put("ELSE", 3);
		this.map.put("FI", 4);
		this.map.put("LOOP", 5);
        this.map.put("BREAK", 6);
        this.map.put("READ", 7);
        this.map.put("PRINT", 8);
        this.map.put("AND", 9);
        this.map.put("OR", 0);
        this.map.put("END", 31);
        this.map.put(".", 11);
        this.map.put(")", 12);
        this.map.put("(", 13);
        this.map.put("/", 14);
        this.map.put("*", 15);
        this.map.put("-", 16);
        this.map.put("+", 17);
        this.map.put("<>", 18);
        this.map.put(">", 19);
        this.map.put(">=", 20);
        this.map.put("=", 21);
        this.map.put("<=", 22);
        this.map.put("<", 23);
        this.map.put(":=", 24);
        this.map.put(";", 25);
        this.map.put("SPACE", 26);
        this.map.put("EOL", 27);
        this.map.put("IDENTIFIER", 28);
        this.map.put("NUMBER", 29);
        this.map.put("STRING", 3);
	}

	public boolean isToken(String tok)
	{
		return this.map.containsKey(tok);
	}

	public int getValue(String tok)
	{
		return this.map.get(tok);
	}
}