/* NOTES:
			*/

import java.util.*;

public class parseToken
{
	public TokenValue tv = new TokenValue();

	// is the single digit a number??
	public boolean isNum (String s)
	{
		return Pattern.matches("^\\d*\\.?\\d+$", s);
	}

	// is this single character a digit??
	public boolean isDigit(String s)
	{
		return Pattern.matches("^\\d$", s);
	}

	public boolean isDeci(String s)
	{
		return s.equals(".");
	}

	public boolean isSpace(String s)
	{
		return s.equals(" ");
	}

	public boolean isEOL (String s)
	{
		return s.equals("\n");
	}
}